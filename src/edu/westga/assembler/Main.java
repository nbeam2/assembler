package edu.westga.assembler;

import java.io.File;

import edu.westga.assembler.controller.Assembler;
import edu.westga.assembler.controller.FileHandler;

/**
 * Main driver class
 * @author Nathan Beam
 * @version Fall 2016
 */
public class Main {

	/**
	 * Main method. program entry point.
	 * @param args hopefull the name of the file
	 */
	public static void main(String[] args) {
		if (args.length > 0) {
			File hackFile = FileHandler.loadFile(args[0]);
			Assembler assembler = new Assembler(hackFile);
			assembler.assemble();
			FileHandler.saveFile(hackFile, assembler);
			System.out.println("Succesfully saved");
		} else {
			System.out.println("Missing file name. Syntax is 'Assembler.jar [filename]'");
		}

	}

}