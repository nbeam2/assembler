package edu.westga.assembler.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

import edu.westga.assembler.model.Code;
import edu.westga.assembler.model.SymbolTable;

/**
 * Main assembler class for translating from Hack to binary
 * 
 * @author Nathan Beam
 * @version Fall 2016
 */
public class Assembler {

	private ArrayList<String> programLines;
	private ArrayList<String> outlines;

	private enum CommandType {
		A_COMMAND, C_COMMAND, L_COMMAND
	}

	private int romCounter = 0;
	private int varCounter = 16;
	private SymbolTable symbolTable;

	/**
	 * Constructor for assembler
	 * 
	 * @param hackFile te file of Hack code to be translated
	 */
	public Assembler(File hackFile) {
		if (hackFile == null) {
			throw new IllegalArgumentException("Hack File cannot be null");
		}
		this.symbolTable = new SymbolTable();
		this.outlines = new ArrayList<String>();
		this.getContents(hackFile);
	}

	/**
	 * Driver method to direct assembly
	 */
	public void assemble() {
		this.buildSymbolTable();
		for (String line : this.programLines) {
			String bin = "";
			if (this.commandType(line) == CommandType.C_COMMAND) {
				bin += "111";
				bin += Code.comp(this.comp(line));
				bin += Code.dest(this.dest(line));
				bin += Code.jump(this.jump(line));
			} else if (this.commandType(line) == CommandType.A_COMMAND) {
				bin += "0";
				bin += this.handleSymbol(this.symbol(line));
			} else if (this.commandType(line) == CommandType.L_COMMAND) {
				bin += "0";
				bin += this.handleSymbol(this.symbol(line));
			} else {
				System.out.print("Syntax error on line " + this.programLines.indexOf(line));
				System.exit(1);
			}
			this.outlines.add(bin);
		}
	}

	/**
	 * Get the output lines translated to binary
	 * @return ArrayList of lines of binary
	 */
	public ArrayList<String> getOutLines() {
		return this.outlines;
	}

	private void buildSymbolTable() {
		ArrayList<String> linesToRemove = new ArrayList<String>();
		for (String line : this.programLines) {
			CommandType ct = this.commandType(line);
			if (ct == CommandType.L_COMMAND) {
				String symbol = this.symbol(line);
				this.symbolTable.addEntry(symbol, this.romCounter);
				linesToRemove.add(line);
			} else {
				this.romCounter++;
			}
		}
		for (String line : linesToRemove) {
			this.programLines.remove(line);
		}

	}

	private CommandType commandType(String line) {
		line = line.trim();
		if (line.startsWith("@")) {
			return CommandType.A_COMMAND;
		} else if (line.startsWith("(")) {
			return CommandType.L_COMMAND;
		} else {
			return CommandType.C_COMMAND;
		}
	}

	private String symbol(String line) {
		if (this.commandType(line) == CommandType.L_COMMAND) {
			return (line.replace('(', ' ').replace(')', ' ').trim());
		} else {
			try {
				return line.substring(1);
			} catch (NumberFormatException n) {
				return "TODO";
			}
		}
	}

	private String dest(String line) {
		if (!line.contains("=")) {
			return "null";
		} else {
			return (line.substring(0, line.indexOf('=')));
		}
	}

	private String comp(String line) {
		int start = 0;
		int end = line.length();
		if (line.contains("=")) {
			start = line.indexOf("=") + 1;
		}
		if (line.contains(";")) {
			end = line.indexOf(";");
		}
		return line.substring(start, end);
	}

	private String jump(String line) {
		if (!line.contains(";")) {
			return "null";
		} else {
			return (line.substring(line.indexOf(';') + 1));
		}
	}

	/**
	 * Best practices? More like stifling creativity!
	 */
	private String handleSymbol(String symbol) {
		String bin = "";
		try {
			int address = Integer.parseInt(symbol);
			bin = Integer.toBinaryString(address);
		} catch (NumberFormatException e) {
			if (!this.symbolTable.contains(symbol)) {
				this.symbolTable.addEntry(symbol, this.varCounter);
				this.varCounter++;
			}
			bin = this.symbolTable.getAddress(symbol);
		}

		for (int iterator = bin.length(); iterator < 15; iterator++) {
			bin = "0" + bin;
		}
		return bin;

	}

	private void getContents(File hackFile) {
		try {
			Scanner scanner = new Scanner(hackFile);
			String fileContents = scanner.useDelimiter("\\Z").next();
			scanner.close();
			this.programLines = new ArrayList<String>();
			String[] temp = fileContents.split("\n");
			for (String line : temp) {
				String newLine = line.trim().replaceAll("\\s", "");
				if (!newLine.startsWith("//") && newLine.length() > 0) {
					this.addLine(newLine);
				}
			}
		} catch (FileNotFoundException e) {
			System.out.println("Invalid or missing file.");
			System.exit(1);
		}

	}

	private void addLine(String newLine) {
		if (newLine.contains("//")) {
			this.programLines.add(newLine.substring(0, newLine.indexOf("//")));
		} else {
			this.programLines.add(newLine);

		}
	}
}