package edu.westga.assembler.controller;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

/**
 * Static class to handle file operations
 * @author Nathan Beam
 * @version Fall 2016
 */
public class FileHandler {

	
	/**
	 * Handles loading of file
	 * @param path the path of the file to load
	 * @return the loaded file
	 */
	public static File loadFile(String path) {
		File loadedFile = new File(path);
		return loadedFile;
	}

	/**
	 * Saves the hack file to the same folder as the asm file, 
	 * with the same name, and a .hack extension
	 * 
	 * @param file the original file, to get path and name
	 * @param assembler assembler to get file contents
	 */
	public static void saveFile(File file, Assembler assembler) {
		String fileName = file.getPath().replace(".asm", ".hack");
		Path filePath = Paths.get(fileName);
		ArrayList<String> outlines = assembler.getOutLines();
		try {
			Files.write(filePath, outlines, Charset.forName("UTF-8"));
		} catch (IOException e) {
			System.out.println("Error saving file. Check matching files are closed and try again.");
		}
	}
}