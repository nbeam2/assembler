package edu.westga.assembler.model;

/**
 * Model class containing predefined constant values and operation codes
 * @author Nathan Beam
 * @version Fall 2016
 */
public class Code {

	/**
	 * Defines operation codes for dest commands
	 * 
	 * @param dest dest symbol e.g. 'AM'
	 * @return The binary representation of each command
	 */
	public static String dest(String dest) {
		switch (dest) {
			case "null":
				return "000";
			case "M":
				return "001";
			case "D":
				return "010";
			case "MD":
				return "011";
			case "A":
				return "100";
			case "AM":
				return "101";
			case "AD":
				return "110";
			case "AMD":
				return "111";
			default:
				throw new IllegalArgumentException("Invalid dest " + dest);
		}
	}

	/**
	 * Defines operation codes for jump commands
	 * 
	 * @param jump jump symbol e.g. 'JEQ'
	 * @return The binary representation of each command
	 */
	public static String jump(String jump) {
		switch (jump) {
			case "null":
				return "000";
			case "JGT":
				return "001";
			case "JEQ":
				return "010";
			case "JGE":
				return "011";
			case "JLT":
				return "100";
			case "JNE":
				return "101";
			case "JLE":
				return "110";
			case "JMP":
				return "111";
			default:
				throw new IllegalArgumentException("Invalid jump " + jump);
		}
	}

	/**
	 * Defines operation codes for comp commands
	 * 
	 * @param comp comp symbol e.g. 'A+M'
	 * @return The binary representation of each command
	 */
	public static String comp(String comp) {
		switch (comp) {
			case "0":
				return "0101010";
			case "1":
				return "0111111";
			case "-1":
				return "0111010";
			case "D":
				return "0001100";
			case "A":
				return "0110000";
			case "!D":
				return "0001101";
			case "!A":
				return "0110001";
			case "-D":
				return "0001111";
			case "-A":
				return "0110011";
			case "D+1":
				return "0011111";
			case "A+1":
				return "0110111";
			case "D-1":
				return "0001110";
			case "A-1":
				return "0110010";
			case "D+A":
				return "0000010";
			case "A+D":
				return "0000010";
			case "D-A":
				return "0010011";
			case "A-D":
				return "0000111";
			case "D&A":
				return "0000000";
			case "D|A":
				return "0010101";
			case "M":
				return "1110000";
			case "!M":
				return "1110001";
			case "-M":
				return "1110011";
			case "M+1":
				return "1110111";
			case "M-1":
				return "1110010";
			case "D+M":
				return "1000010";
			case "D-M":
				return "1010011";
			case "M-D":
				return "1000111";
			case "D&M":
				return "1000000";
			case "D|M":
				return "1010101";
			default:
				throw new IllegalArgumentException("Invalid comp " + comp);

		}
	}

}