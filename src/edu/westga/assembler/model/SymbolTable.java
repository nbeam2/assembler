package edu.westga.assembler.model;

import java.util.Hashtable;

/**
 * Handles symbol table for program, storing 
 * user defined values and variables. 
 * 
 * @author Nathan Beam
 * @version Fall 2016
 */
public class SymbolTable {

	private Hashtable<String, Integer> addresses;

	/**
	 * Constructor to instantiate SymbolTable
	 */
	public SymbolTable() {
		this.addresses = new Hashtable<String, Integer>();
		this.loadConstants();
	}

	/**
	 * Adds new Symbol:Address entry into SymbolTable
	 * @param symbol the string symbol of the address
	 * @param address decimal address
	 */
	public void addEntry(String symbol, int address) {
		if (!this.addresses.containsKey(symbol)) {
			this.addresses.put(symbol, address);
		}
	}

	/**
	 * Determines whether the specified symbol exists already in the table
	 * @param symbol the symbol to check
	 * @return whether symbol already has an address in the table
	 */
	public boolean contains(String symbol) {
		return this.addresses.containsKey(symbol);
	}

	/**
	 * Get the address of a given symbol
	 * @param symbol the string symbol to get address of
	 * @return binary representation of address
	 */
	public String getAddress(String symbol) {
		int addr = this.addresses.get(symbol);
		return Integer.toBinaryString(addr);
	}

	private void loadConstants() {
		this.addEntry("SP", 0);
		this.addEntry("LCL", 1);
		this.addEntry("ARG", 2);
		this.addEntry("THIS", 3);
		this.addEntry("THAT", 4);
		this.addEntry("R0", 0);
		this.addEntry("R1", 1);
		this.addEntry("R2", 2);
		this.addEntry("R3", 3);
		this.addEntry("R4", 4);
		this.addEntry("R5", 5);
		this.addEntry("R6", 6);
		this.addEntry("R7", 7);
		this.addEntry("R8", 8);
		this.addEntry("R9", 9);
		this.addEntry("R10", 10);
		this.addEntry("R11", 11);
		this.addEntry("R12", 12);
		this.addEntry("R13", 13);
		this.addEntry("R14", 14);
		this.addEntry("R15", 15);
		this.addEntry("SCREEN", 16384);
		this.addEntry("KBD", 24576);
	}
}
